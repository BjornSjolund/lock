package main

import (
	"fmt"
	"testing"
)

func TestParseBeaconBytes(t *testing.T) {

	adv := []byte{76, 0, 2, 21, 104, 27, 164, 111, 70, 57, 72, 19, 184, 43, 14, 118, 232, 123, 88, 94, 0, 1, 0, 1, 180}

	result, err := parseBeaconBytes(adv)

	if err != nil {
		t.Error("Failed to parse bytes", err)
	}

	fmt.Println(result)
}
