package main

import (
	"encoding/binary"
	"encoding/hex"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/luismesas/goPi/piface"
	"github.com/luismesas/goPi/spi"

	"github.com/paypal/gatt"
	"github.com/paypal/gatt/examples/option"
	"github.com/paypal/gatt/examples/service"
)

const theMagicWord = "opensesame"

var (
	unlocked bool
	scanMode bool
	unlockTimestamp time.Time
)

// Beacon is the beacon
type Beacon struct {
	UUID  string
	Major uint16
	Minor uint16
}

func parseBeaconBytes(adv []byte) (Beacon, error) {
	if len(adv) < 25 {
		return Beacon{}, errors.New("Not a beacon")
	}

	uuid := hex.EncodeToString(adv[4:20])

	//Get the Major
	major := binary.BigEndian.Uint16(adv[20:22])

	//Get the Minor
	minor := binary.BigEndian.Uint16(adv[22:24])

	beacon := Beacon{UUID: uuid, Major: major, Minor: minor}

	return beacon, nil
}

//Lock is used for indicating the state of the lock
type Lock struct {
	Unlocked bool `json:"unlocked"`
}

func main() {

	flag.BoolVar(&scanMode, "scan", false, "The service will scan for the WB UUID and Major 1337 and Minor 1337")
	flag.Parse()
	unlockTimestamp = time.Now()
	if scanMode {

		log.Println("WARNING! Running in scan mode...")

		startScanMode()

	} else {

		log.Println("Running in server mode...")

		//start the beacon service
		go startBeacon()

		//Start the web service
		router := mux.NewRouter().StrictSlash(true)
		router.HandleFunc("/", Index)
		router.HandleFunc("/lock", LockStatus)
		router.HandleFunc("/unlock/{lockID}", Unlock)

		log.Fatal(http.ListenAndServe(":8080", router))
	}
}

//Index show a small welcome message :)
func Index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Go away, please!")
}

//LockStatus Get the current status of the lock
func LockStatus(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Locked!")
}

// Unlock Unlock the lock
func Unlock(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	lockID := vars["lockID"]
	var lock Lock

	if lockID == theMagicWord && !unlocked {
		lock = Lock{Unlocked: true}
		triggerUnlock()
	} else {
		lock = Lock{Unlocked: false}
	}

	json.NewEncoder(w).Encode(lock)

}

// Trigger the piFace relays to unlock the door
func triggerUnlock() {


	if int(time.Now().Sub(unlockTimestamp).Seconds()) < 4{
		log.Printf("Time diff: %d\n",int(time.Now().Sub(unlockTimestamp).Seconds()))
		return	
	}

		
	unlockTimestamp = time.Now()
	
	// creates a new pifacedigital instance
	pfd := piface.NewPiFaceDigital(spi.DEFAULT_HARDWARE_ADDR, spi.DEFAULT_BUS, spi.DEFAULT_CHIP)

	// initializes pifacedigital board
	err := pfd.InitBoard()
	if err != nil {
		fmt.Printf("Error on init board: %s", err)
		return
	}
	unlocked = true
	pfd.Relays[0].Toggle()
	time.Sleep(time.Second)
	pfd.Relays[0].Toggle()
	unlocked = false

}

func startScanMode() {
	d, err := gatt.NewDevice(option.DefaultServerOptions...)

	if err != nil {
		log.Fatalf("Failed to open device, err: %s", err)
	}

	// Register handlers.
	d.Handle(gatt.PeripheralDiscovered(onPeriphDiscovered))
	d.Init(onStateChanged)
	select {}
}

func onPeriphDiscovered(p gatt.Peripheral, a *gatt.Advertisement, rssi int) {

	beacon, err := parseBeaconBytes(a.ManufacturerData)

	if err != nil {
		//fmt.Println(err.Error())
	} else {
		if beacon.Major == 1337 && beacon.Minor == 1337 {
			log.Println("Someone is really close, checking if they are in range")

			if p.ReadRSSI() > -60 {
				log.Printf("RSSI: %d\n", p.ReadRSSI())
				log.Println("They are also in range! Unlocking...")

				triggerUnlock()
			}
		}
	}

}

func onStateChanged(d gatt.Device, s gatt.State) {
	fmt.Println("State:", s)
	switch s {
	case gatt.StatePoweredOn:
		fmt.Println("scanning...")
		d.Scan([]gatt.UUID{}, true)
		return
	default:
		fmt.Println("Stopping..")
		d.StopScanning()
	}
}

func startBeacon() {
	d, err := gatt.NewDevice(option.DefaultServerOptions...)
	if err != nil {
		log.Fatalf("Failed to open device, err: %s", err)
	}

	// Register optional handlers.
	d.Handle(
		gatt.CentralConnected(func(c gatt.Central) { fmt.Println("Connect: ", c.ID()) }),
		gatt.CentralDisconnected(func(c gatt.Central) { fmt.Println("Disconnect: ", c.ID()) }),
	)

	// A mandatory handler for monitoring device state.
	onStateChanged := func(d gatt.Device, s gatt.State) {
		fmt.Printf("State: %s\n", s)
		switch s {
		case gatt.StatePoweredOn:
			// Setup GAP and GATT services for Linux implementation.
			// OS X doesn't export the access of these services.
			d.AddService(service.NewGapService("Gopher")) // no effect on OS X
			d.AddService(service.NewGattService())        // no effect on OS X

			// Advertise as an OpenBeacon iBeacon
			d.AdvertiseIBeacon(gatt.MustParseUUID("681BA46F46394813B82B0E76E87B585E"), 1, 1337, -59)

		default:
		}
	}

	d.Init(onStateChanged)
	select {}
}
